package com.awd.examen_1b

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import kotlin.random.Random

class MainActivity : AppCompatActivity() {
    private lateinit var countDownTimer: CountDownTimer
    private lateinit var startButton: Button
    private lateinit var tapButton: Button
    private lateinit var mensajeTextView: TextView
    private lateinit var scoreTextView: TextView

    private var initialCountDown: Long = 10000
    private var countDownInterval: Long = 1000

    private var timeLeft = 10
    private var isGameStarted = false

    private var secondSelected = 0
    private  var score = 0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        startButton = findViewById(R.id.start_button)
        tapButton = findViewById(R.id.tap_button)
        mensajeTextView  = findViewById(R.id.mensaje_textView)
        scoreTextView =   findViewById(R.id.score_textView)
        startButton.setOnClickListener{gameStart()}
        tapButton.setOnClickListener{tapButton()}
        resetGame()
    }


    private fun resetGame(){
        secondSelected = Random.nextInt(0,10)
        timeLeft=10
        score = -1
        scoreTextView.text = getString(R.string.score,score)
        mensajeTextView.text = getString(R.string.mensaje,"Dont start game!")


        countDownTimer = object: CountDownTimer(initialCountDown,countDownInterval){
            override fun onFinish() {
                endGame()
            }

            override fun onTick(millisUntilFinished: Long) {
                timeLeft = millisUntilFinished.toInt()/1000
            }
        }
        isGameStarted = false
    }

    private fun  tapButton(){
        if(isGameStarted){
            if(timeLeft == secondSelected){
                score = 100
                mensajeTextView.text = getString(R.string.mensaje,"WIN!")
                scoreTextView.text = getString(R.string.score,score)

            }else if (timeLeft+1 == secondSelected || timeLeft-1 == secondSelected){
                score = 50
                mensajeTextView.text = getString(R.string.mensaje,"Casi Ganas!")
                scoreTextView.text = getString(R.string.score,score)

            }else{
                score = 0
                mensajeTextView.text = getString(R.string.mensaje,"Lose!")
                scoreTextView.text = getString(R.string.score,score)
            }

        }else{
            Toast.makeText(this,getString(R.string.inicie), Toast.LENGTH_LONG).show()
        }
    }
    private  fun gameStart(){
        countDownTimer.start()
        mensajeTextView.text = getString(R.string.mensaje,"game started!")
        isGameStarted = true
    }

    private fun  endGame(){
    resetGame()
    }
}
